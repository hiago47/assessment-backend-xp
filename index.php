<?php

require_once 'vendor/autoload.php';

use App\Controllers\ProductController;
use App\Controllers\CategoryController;

$request_uri = $_SERVER['REQUEST_URI'];

if(!empty($_GET)) {
    $_url = explode('?', $request_uri);
    $request_uri = $_url[0];
}

$arrParam = explode('/', $request_uri);
$arrParam = array_values(array_filter($arrParam));

$url = '';
$idParam = null;

$cParam = count($arrParam);
if($cParam > 3) {
    pageNotFound();
}

if($cParam === 1) {

    $url = $arrParam[0];
}
else if($cParam === 2) {

    $url = $arrParam[0] . '/' . $arrParam[1];
}
else if($cParam === 3) {

    if(!preg_match('/^[0-9]+$/', $arrParam[2])) {
        pageNotFound();
    }
    
    $idParam = $arrParam[2];
    $url = $arrParam[0] . '/' . $arrParam[1] . '/' . $idParam;
}

switch($url) {
    case '' :
        
        $productCtrl = new ProductController;
        $productCtrl->dashboard();

        break;
    case 'products' :

        $productCtrl = new ProductController;
        $productCtrl->list();

        break;
    case 'categories' :

        $categoryCtrl = new CategoryController;
        $categoryCtrl->list();

        break;
    case 'products/new' :

        $productCtrl = new ProductController;
        $productCtrl->new();

        break;
    case 'categories/new' :

        $categoryCtrl = new CategoryController;
        $categoryCtrl->new();

        break;
    case 'products/edit/' . $idParam :

        $productCtrl = new ProductController;
        $productCtrl->edit($idParam);

        break;
    case 'products/delete/' . $idParam :

        $productCtrl = new ProductController;
        $productCtrl->delete($idParam);

        break;
    case 'categories/edit/' . $idParam :

        $categoryCtrl = new CategoryController;
        $categoryCtrl->edit($idParam);

        break;
    case 'categories/delete/' . $idParam :

        $categoryCtrl = new CategoryController;
        $categoryCtrl->delete($idParam);

        break;
    default :

        pageNotFound();

        break;
}
?>