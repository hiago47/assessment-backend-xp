$(document).ready(function() {
    
    $(document).on("click", ".delete-item", function () {

        if(confirm('Are you sure you want to delete this item?')) {
            document.location = $(this).data('route');
        }

    });

});
