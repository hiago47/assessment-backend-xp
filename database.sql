CREATE DATABASE IF NOT EXISTS `gojumpers`;
USE `gojumpers`;


CREATE TABLE IF NOT EXISTS `products` (
	`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	`price` float(10,2) NOT NULL,
	`quantity` int(11) unsigned NOT NULL,
	`description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp(),
	`deleted_at` datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `categories` (
	`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp(),
	`deleted_at` datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `products_categories` (
	`product_id` bigint(20) unsigned NOT NULL,
	`category_id` bigint(20) unsigned NOT NULL,
	UNIQUE KEY `product_id_category_id` (`product_id`,`category_id`) USING BTREE,
	KEY `fk_category_id` (`category_id`) USING BTREE,
	CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
	CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `logs` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`class` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`registry_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`test` BIT(1) NOT NULL DEFAULT b'0',
	`registered` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

