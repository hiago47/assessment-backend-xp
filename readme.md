# Resposta ao desafio WebJump

# O Sistema 
Gerenciador de produtos e categorias.

# Versão da linguagem e padrão de desenvolvimento 
Desenvolvido na linguagem PHP versão 7.4, banco de dados MySQL e rodando em servidor local Apache. 
Foi utilizado o padrão PSR-4 para o carregamento das classes.

# Estrutura de Pastas 
- app:
	- Controllers, 
	- Models, 
	- **helpers.php** (com funções básicas de uso geral)
- assets:
	- css, js e imagens
- includes:
	- html (views)

# Rotas 
- São definidas no arquivo **index.php** 
- :
	- / : Dashboard
	- /products : Listagem de todos os produtos
	- /categories : Listagem de todas os categorias
	- /products/new : Cadastrar novo produto
	- /categories/new : Cadastrar nova categoria

# Banco de dados
- Toda a estrutura encontra-se no arquivo: **database.sql** 
- Basta importa-lo no seu servidor e será criado o banco (nome gojumpers) e as devidas tabelas vazias
- As tabelas usam o esquema de soft delete
- Todas as ações são gravadas na tabela "logs"

# Instalação
- Devido ao **.htaccess**, certifique-se que os arquivos fiquem na pasta raiz do servidor ou utilize um Virtual Host
- Em **config.php** poderá configurar a url do app e o acesso ao banco de dados importado
- Execute: 
```composer install```

