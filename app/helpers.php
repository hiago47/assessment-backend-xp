<?php

function redirect($url) {
    header("Location: ".APP_URL."$url");
    exit();
}

function pageNotFound() {
    include_once 'includes/404.php';
    exit();
}

function moneyFormat($val=0) {
    return number_format($val, 2, ",", ".");
}

