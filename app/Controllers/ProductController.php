<?php 

namespace App\Controllers;
use App\Models\Product;
use App\Models\Category;

class ProductController
{
    public function list() 
    {
        $product = new Product;

        $items = [];
        $res = $product->list();
        if($res) {
            $items = $res;
        }

        include_once 'includes/product_list.php';
    }

    public function new() 
    {
        $product = new Product;

        if(empty($_POST)) {

            $category = new Category;
            $arrCategories = [];
            $res = $category->list();
            if($res) {
                $arrCategories = $res;
            }

            $title = "New Product";
            $action = APP_URL."/products/new";
            include_once 'includes/product_form.php';
        }
        else {
            
            $product->setName($_POST['name']);
            $product->setSku($_POST['sku']);
            $product->setPrice($_POST['price']);
            $product->setQuantity($_POST['quantity']);
            $product->setDescription($_POST['description']);
            $product->setCategories($_POST['categories']);

            $product->insert();

            redirect('/products');
        }
    }

    public function edit($id)
    {
        $product = new Product;

        if(empty($_POST)) {

            $product->setId($id);

            if($product->find()) {
                $category = new Category;
                $arrCategories = [];
                $res = $category->list();
                if($res) {
                    $arrCategories = $res;
                }

                $title = "Edit Product";
                $action = APP_URL."/products/edit/$id";
                include_once 'includes/product_form.php';
            }
            else {
                redirect('/products');
            }
        }
        else {

            $product->setId($_POST['id']);
            $product->setName($_POST['name']);
            $product->setSku($_POST['sku']);
            $product->setPrice($_POST['price']);
            $product->setQuantity($_POST['quantity']);
            $product->setDescription($_POST['description']);
            $product->setCategories($_POST['categories']);

            $product->update();

            redirect('/products');
        }
    }

    public function delete($id)
    {
        $product = new Product;
        $product->setId($id);

        $product->delete();

        redirect('/products');
    }

    public function dashboard()
    {
        $product = new Product;

        $arrProducts = [];
        $res = $product->list();
        if($res) {
            $arrProducts = $res;
        }

        $totalOfProducts = count($arrProducts);

        include_once 'includes/dashboard.php';
    }
}