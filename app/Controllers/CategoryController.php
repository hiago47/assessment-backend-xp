<?php 

namespace App\Controllers;
use App\Models\Category;

class CategoryController
{
    public function list() 
    {
        $category = new Category;

        $items = [];
        $res = $category->list();
        if($res) {
            $items = $res;
        }
        
        include_once 'includes/category_list.php';
    }

    public function new() 
    {
        $category = new Category;

        if(empty($_POST)) {

            $title = "New Category";
            $action = APP_URL."/categories/new";
            include_once 'includes/category_form.php';
        }
        else {
            
            $category->setName($_POST['name']);
            $category->setCode($_POST['code']);

            $category->insert();

            redirect('/categories');
        }
    }

    public function edit($id)
    {
        $category = new Category;

        if(empty($_POST)) {

            $category->setId($id);

            if($category->find()) {

                $title = "Edit Category";
                $action = APP_URL."/categories/edit/$id";
                include_once 'includes/category_form.php';
            }
            else {
                redirect('/categories');
            }
        }
        else {

            $category->setId($_POST['id']);
            $category->setName($_POST['name']);
            $category->setCode($_POST['code']);

            $category->update();

            redirect('/categories');
        }
    }

    public function delete($id)
    {
        $category = new Category;
        $category->setId($id);

        $category->delete();

        redirect('/categories');
    }

    public function listDashboard()
    {

    }


}