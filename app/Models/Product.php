<?php 

namespace App\Models;

class Product extends DB
{
    private $id = "";
    private $name = "";
    private $sku = "";
    private $price = "";
    private $quantity = "";
    private $description = "";
    private $categories = [];

    public function list($num = null) 
    {
        $this->sql = "SELECT 
        P.id, P.name, P.sku, P.price, P.quantity,
        (
            SELECT CONCAT(
                '[', 
                GROUP_CONCAT(JSON_OBJECT('name', C.name)),
                ']'
            ) 
            FROM products_categories AS PC 
            INNER JOIN categories AS C 
            ON(PC.category_id = C.id AND C.deleted_at IS NULL)
            WHERE PC.product_id = P.id 
        ) AS categories 
        FROM products AS P 
        WHERE deleted_at IS NULL 
        ORDER BY id DESC";

        if(!is_null($num)) {
            $this->sql.= " LIMIT $num";
        }

        return $this->query();
    }

    public function find()
    {
        $this->sql = "SELECT 
        P.id, P.name, P.sku, P.price, P.quantity, P.description, 
        GROUP_CONCAT(PC.category_id) AS categories 
        FROM products AS P 
        LEFT JOIN products_categories AS PC 
        ON (P.id = PC.product_id) 
        WHERE 
        P.id = :id AND P.deleted_at IS NULL";

        $this->param = [':id' => $this->getId()];

        $res = $this->query();
        
        if(!$res || empty($res[0]['id'])) {
            return false;
        }

        $this->name = $res[0]['name'];
        $this->sku = $res[0]['sku'];
        $this->price = $res[0]['price'];
        $this->quantity = $res[0]['quantity'];
        $this->description = $res[0]['description'];
        $this->categories = [];
        if(!empty($res[0]['categories'])) {
            $this->categories = explode(',', $res[0]['categories']);
        }

        return true;
    }

    public function insert()
    {
        $this->sql = "INSERT INTO products (
            name, sku, price, quantity, description 
        )
        VALUES (
            :name, :sku, :price, :quantity, :description
        )";

        $this->param = [
            ':name' => $this->name,
            ':sku' => $this->sku,
            ':price' => $this->price,
            ':quantity' => $this->quantity,
            ':description' => $this->description
        ];

        if(!$this->execute()) {
            return false;
        }
        
        $this->id = $this->getLastInsertId();

        $this->log('INSERT');

        return $this->insertProductCategories();
    }

    public function update()
    {
        $this->sql = "UPDATE products SET 
        name = :name, sku = :sku, price = :price, 
        quantity = :quantity, description = :description 
        WHERE id = :id";

        $this->param = [
            ':name' => $this->name,
            ':sku' => $this->sku,
            ':price' => $this->price,
            ':quantity' => $this->quantity,
            ':description' => $this->description,
            ':id' => $this->id
        ];

        if(!$this->execute()) {
            return false;
        }

        $this->log('UPDATE');

        return $this->insertProductCategories();
    }

    public function delete()
    {
        $this->sql = "UPDATE products SET 
        deleted_at = current_timestamp() 
        WHERE id = :id";

        $this->param = [':id' => $this->getId()];

        if(!$this->execute()) {
            return false;
        }

        $this->log('DELETE');

        return true;
    }

    public function insertProductCategories()
    {
        $this->sql = "DELETE FROM products_categories WHERE product_id = :prod_id";

        $this->param = [':prod_id' => $this->getId()];

        if(!$this->execute()) {
            return false;
        }

        if(!empty($this->categories)) {
            
            $this->sql = "INSERT INTO products_categories(product_id, category_id)
            VALUES ";

            $this->param = [];
            $this->param[':prod_id'] = $this->getId();

            foreach($this->categories as $idCategory) {
                $bind = ':cat_' . $idCategory . 'id';
                $this->sql.= "(:prod_id, ". $bind ."),";

                $this->param[$bind] = $idCategory;
            }

            $this->sql = rtrim($this->sql, ",");           

            if(!$this->execute()) {
                return false;
            }
        }

        return true;
    }

    public function log($type)
    {
        $this->sql = "INSERT INTO logs (
            class, type, registry_id, test 
        )
        VALUES (
            :class, :type, :registry_id, :test
        )";

        $this->param = [
            ':class' => get_class(),
            ':type' => $type,
            ':registry_id' => $this->id,
            ':test' => (isset($this->isTest) ? $this->isTest : false)
        ];

        $this->execute();
    }

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getSku()
    {
        return $this->sku;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getQuantity()
    {
        return $this->quantity;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getCategories()
    {
        return $this->categories;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setSku($sku)
    {
        $this->sku = $sku;
    }
    public function setPrice($price)
    {
        $this->price = str_replace(",", ".", $price);
    }
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    public function setDescription($description) 
    {
        $this->description = $description;
    }
    public function setCategories($categories) 
    {
        $this->categories = $categories;
    }

}