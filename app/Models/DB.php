<?php 
namespace App\Models;

use PDO;
use PDOException;

class DB
{
    private $conn;
    protected $sql = "";
    protected $param = [];

    public function __construct()
    {
        try
        {
            $this->conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USERNAME, DB_PASSWORD);
        }
        catch (PDOException $e)
        {
            die("Database Connection Error: " . $e->getMessage());
        }
    }

    protected function query() 
    {
        $stmt = $this->conn->prepare($this->sql);

        if(!empty($this->param)) {
            foreach($this->param as $column => $value) {
                $stmt->bindValue($column, $value);
            }
        }

        if($stmt->execute()) {
            return $stmt->fetchAll($this->conn::FETCH_ASSOC);
        }
        else {
            return false;
        }
    }

    protected function execute() 
    {
        $stmt = $this->conn->prepare($this->sql);

        if(!empty($this->param)) {
            foreach($this->param as $column => $value) {
                $stmt->bindValue($column, $value);
            }
        }

        if($stmt->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

    protected function getLastInsertId() 
    {
        return $this->conn->lastInsertId();
    }

    
}