<?php 

namespace App\Models;

class Category extends DB
{
    private $id = "";
    private $name = "";
    private $code = "";

    public function list($num = null) 
    {
        $this->sql = "SELECT 
        id, name, code 
        FROM categories 
        WHERE deleted_at IS NULL 
        ORDER BY id DESC";

        if(!is_null($num)) {
            $this->sql.= " LIMIT $num";
        }

        return $this->query();
    }

    public function find()
    {
        $this->sql = "SELECT 
        id, name, code  
        FROM categories 
        WHERE id = :id AND deleted_at IS NULL";

        $this->param = [':id' => $this->getId()];

        $res = $this->query();

        if(!$res || empty($res)) {
            return false;
        }

        $this->name = $res[0]['name'];
        $this->code = $res[0]['code'];

        return true;
    }

    public function insert()
    {
        $this->sql = "INSERT INTO categories (
            name, code 
        )
        VALUES (
            :name, :code 
        )";

        $this->param = [
            ':name' => $this->name,
            ':code' => $this->code
        ];

        if(!$this->execute()) {
            return false;
        }

        $this->id = $this->getLastInsertId();

        $this->log('INSERT');

        return true;
    }

    public function update()
    {
        $this->sql = "UPDATE categories SET 
        name = :name, code = :code 
        WHERE id = :id";

        $this->param = [
            ':name' => $this->name, 
            ':code' => $this->code,
            ':id' => $this->id
        ];

        if(!$this->execute()) {
            return false;
        }

        $this->log('UPDATE');

        return true;
    }

    public function delete()
    {
        $this->sql = "UPDATE categories SET 
        deleted_at = current_timestamp() 
        WHERE id = :id";

        $this->param = [':id' => $this->getId()];

        if(!$this->execute()) {
            return false;
        }

        $this->log('DELETE');

        return true;
    }

    public function log($type)
    {
        $this->sql = "INSERT INTO logs (
            class, type, registry_id, test 
        )
        VALUES (
            :class, :type, :registry_id, :test
        )";

        $this->param = [
            ':class' => get_class(),
            ':type' => $type,
            ':registry_id' => $this->id,
            ':test' => (isset($this->isTest) ? $this->isTest : false)
        ];

        $this->execute();
    }

    public function getId() 
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getCode()
    {
        return $this->code;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setCode($code)
    {
        $this->code = $code;
    }
}