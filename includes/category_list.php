<?php 
    $title = "Categories";
    include_once 'header.php';
?>

<div class="header-list-page">
    <h1 class="title"><?= $title ?></h1>
    <a href="<?= APP_URL ?>/categories/new" class="btn-action">Add new Category</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
    </tr>

    <?php 
    foreach ($items as $item) {

    ?>

    <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['name'] ?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['code'] ?></span>
        </td>
        <td class="data-grid-td">
            <div class="actions">
                <div class="action edit">
                    <a href="<?= APP_URL ?>/categories/edit/<?= $item['id'] ?>"><span>Edit</span></a>
                </div>
                <div class="action delete">
                    <a href="#" class="delete-item" data-route="<?= APP_URL ?>/categories/delete/<?= $item['id'] ?>"><span>Delete</span></a>
                </div>
            </div>
        </td>
    </tr>

    <?php 
    }
    ?>

</table>

<?php 
    include_once 'footer.php';
?>