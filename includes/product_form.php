<?php 
    include_once 'header.php';
?>

<h1 class="title new-item"><?= $title ?></h1>

<form action="<?= $action ?>" method="post">
    <input type="hidden" name="id" value="<?= $product->getId() ?>"/>
    <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" value="<?= $product->getSku() ?>" maxlength="255"/> 
    </div>
    <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" value="<?= $product->getName() ?>" maxlength="255" required/> 
    </div>
    <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" pattern="[0-9]+([\.,][0-9]{2})?" name="price" class="input-text" value="<?= $product->getPrice() ?>" required/> 
    </div>
    <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" pattern="\d*" name="quantity" class="input-text" value="<?= $product->getQuantity() ?>" required/> 
    </div>
    <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="categories[]" class="input-text">

            <?php 
            foreach($arrCategories as $category) {
                $selected = (in_array($category['id'], $product->getCategories()) ? 'selected' : '');
            ?>

                <option value="<?= $category['id'] ?>" <?= $selected ?>><?= $category['name'] ?></option>
                
            <?php
            }
            ?>
            
        </select>
    </div>
    <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"><?= $product->getDescription() ?></textarea>
    </div>
    <div class="actions-form">
        <a href="<?= APP_URL ?>/products" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
    </div>
</form>

<?php 
    include_once 'footer.php';
?>