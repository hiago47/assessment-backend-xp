<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GoJumpers<?= (isset($title) ? " - $title" : "") ?></title>
    <link  rel="stylesheet" type="text/css" media="all" href="<?= APP_URL ?>/assets/css/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<body>
    <amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
        <div class="close-menu">
            <a on="tap:sidebar.toggle">
                <img src="<?= APP_URL ?>/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
            </a>
        </div>
        <a href="/"><img src="<?= APP_URL ?>/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
        <div>
            <ul>
                <li><a href="<?= APP_URL ?>/categories" class="link-menu">Categories</a></li>
                <li><a href="<?= APP_URL ?>/products" class="link-menu">Products</a></li>
            </ul>
        </div>
    </amp-sidebar>
    <header>
        <div class="go-menu">
            <a on="tap:sidebar.toggle">☰</a>
            <a href="<?= APP_URL ?>" class="link-logo"><img src="<?= APP_URL ?>/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
        </div>
        <div class="right-box">
            <span class="go-title">Administration Panel</span>
        </div>
    </header>
<main class="content">