<?php 
    include_once 'header.php';
?>

<h1 class="title new-item"><?= $title ?></h1>

<form action="<?= $action ?>" method="post">
    <input type="hidden" name="id" value="<?= $category->getId() ?>"/>
    <div class="input-field">
        <label for="name" class="label">Category Name</label>
        <input type="text" id="name" name="name" class="input-text" value="<?= $category->getName() ?>" maxlength="255" required/> 
    </div>
    <div class="input-field">
        <label for="code" class="label">Category Code</label>
        <input type="text" id="code" name="code" class="input-text" value="<?= $category->getCode() ?>" maxlength="255"/> 
    </div>
    <div class="actions-form">
        <a href="<?= APP_URL ?>/categories" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Category" />
    </div>
</form>

<?php 
    include_once 'footer.php';
?>