<?php 
    $title = "Home";
    include_once 'header.php';
?>

<div class="header-list-page">
    <h1 class="title">Dashboard</h1>
</div>
<div class="infor">
    <?= (
        $totalOfProducts > 0 
        ? "You have $totalOfProducts products added on this store:"
        : "You don't have any products yet:"
    ) ?>
    <a href="<?= APP_URL ?>/products/new" class="btn-action">Add new Product</a>
</div>

<ul class="product-list">

<?php 
$count = 0;

foreach($arrProducts as $product) {

    if($count === 4) {
        ?>

        </ul>
        <ul class="product-list">

        <?php
        $count = 0;
    }
    
?>

            <li>
                <div class="product-image">
                    <img src="<?= APP_URL ?>/assets/images/product/product_default.jpg" layout="responsive" width="164" height="145" alt="<?= $product['name'] ?>" />
                </div>
                <div class="product-info">
                    <div class="product-name"><span><?= $product['name'] ?></span></div>
                    <div class="product-price">
                        <span class="special-price">
                            <?= ($product['quantity'] > 0 ? "{$product['quantity']} available" : "Out of stock") ?>
                        </span> 
                        <span>
                            R$<?= moneyFormat($product['price']) ?>
                        </span>
                    </div>
                </div>
            </li>

<?php 
    $count++;

}
?>
</ul>

<?php 
    include_once 'footer.php';
?>