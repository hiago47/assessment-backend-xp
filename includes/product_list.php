<?php 
    $title = "Products";
    include_once 'header.php';
?>

<div class="header-list-page">
    <h1 class="title"><?= $title ?></h1>
    <a href="<?= APP_URL ?>/products/new" class="btn-action">Add new Product</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
    </tr>

    <?php 
    foreach ($items as $item) {
    ?>

    <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['name'] ?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['sku'] ?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= moneyFormat($item['price']) ?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $item['quantity'] ?></span>
        </td>
        <td class="data-grid-td">
            
            <?php 
            if(!empty($item['categories'])) {
                $arrCat = json_decode($item['categories'], true);
                foreach($arrCat as $cat) {
            ?>
                <span class="data-grid-cell-content"><?= $cat['name'] ?></span>
                <br>
            <?php 
                }
            }
            ?>
           
        </td>
        <td class="data-grid-td">
            <div class="actions">
                <div class="action edit">
                    <a href="<?= APP_URL ?>/products/edit/<?= $item['id'] ?>"><span>Edit</span></a>
                </div>
                <div class="action delete">
                    <a href="#" class="delete-item" data-route="<?= APP_URL ?>/products/delete/<?= $item['id'] ?>"><span>Delete</span></a>
                </div>
            </div>
        </td>
    </tr>

    <?php 
    }
    ?>

</table>

<?php 
    include_once 'footer.php';
?>